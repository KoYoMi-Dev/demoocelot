﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace EventsApi.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    namespace EventsApi.Controllers
    {
        [ApiController]
        [Route("api/events")]
        public class EventsController : ControllerBase
        {
            [HttpGet, Route("")]
            public ActionResult<IEnumerable<string>> Get()
            {
                return new string[] { "Event01", "Event02", "Event03" };
            }
        }
    }

}
